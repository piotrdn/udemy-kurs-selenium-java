import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.List;

public class BasicActionTests {

    @Test
    public void actionTests1() {

        ChromeOptions options = new ChromeOptions();
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://testeroprogramowania.github.io/selenium/");
        WebElement pdstStrTestLink = driver.findElement(By.xpath("//a[@href='basics.html' and text()='Podstawowa strona testowa']"));
        //lub tak: WebElement linkTextMethod = driver.findElement(By.linkText("Podstawowa strona testowa"));
        System.out.println(pdstStrTestLink.getText()); //można wyciągnąć tekst z WebElementu który jest pomiędzy znacznikami np <label> tekst do pobrania </label>
        pdstStrTestLink.click();

        WebElement firstButton = driver.findElement(By.id("clickOnMe"));
        //firstButton.click();
        WebElement firstNameField = driver.findElement(By.id("fname"));
        firstNameField.click();
        firstNameField.sendKeys("MojTekst");

        WebElement usernameField = driver.findElement(By.xpath("//input[@type='text' and @name='username']"));
        usernameField.clear();
        usernameField.sendKeys("Piotrek");

        //naciskanie klawiszy na klawiaturze
        usernameField.sendKeys(Keys.TAB);
        WebElement passwordField = driver.findElement(By.xpath("//input[@type='password' and @name='password']"));
        passwordField.sendKeys("Pass123456789098");
        //passwordField.sendKeys(Keys.ENTER); //wykomentowane bo otwiera alert

        WebElement radioCheckButton = driver.findElement(By.xpath("//input[@type='radio' and @value='male']"));
        radioCheckButton.click();

        WebElement radioCheckButton2 = driver.findElement(By.cssSelector("input[value='other']"));
        radioCheckButton2.click();

        WebElement selectBar1 = driver.findElement(By.cssSelector("select"));
        WebElement selectBar2 = driver.findElement(By.xpath("//select//option[@value='volvo' and text()='Volvo']"));
        WebElement selectBar3 = driver.findElement(By.cssSelector("[value='volvo'"));
        Select car = new Select(selectBar1);
        car.selectByValue("volvo");

        //jak pobrać i wypisac wszystkie dostępne opcje dostępne w select:
        List<WebElement> carList = car.getOptions();
        System.out.println("Liczba wszystkich opcji w select car: " + carList.size());
        for(WebElement option : carList) {
            System.out.println("Element: " + option.getText());
        }

        //driver.quit();

    }
}
