import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class WebDriverManagerTest {

// testy dla przykładowej strony:
// https://testeroprogramowania.github.io/selenium/basics.html?username=Mickey&password=Mouse

    @Test
    public void openBrowser() {
        //https://github.com/bonigarcia/webdrivermanager

        WebDriverManager.chromedriver().setup();

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://google.pl");

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("window.open('https://www.google.com','blank','height=200', 'width=200')");

        driver.close();
        //driver.quit();
    }
    @Test
    public void secondest() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
        //options.setHeadless(false);
        WebDriver driver = new ChromeDriver(options);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("alert('Hello alert xD')");
        driver.get("https://google.com");

    }

    @Test
    public void thirdTest() {
        SeleniumTest seleniumTest = new SeleniumTest();

        WebDriver driver = seleniumTest.getDriver("chrome");
        driver.manage().window().maximize();
        driver.get("https://www.google.com");
        // znalezienie przycisku
        WebElement agreeButton = driver.findElement(By.xpath("//div[text()='Zaakceptuj wszystko']"));
// klikniecie przycisku
        agreeButton.click();
        // znajdz pole wyszukiwania
        WebElement searchField = driver.findElement(By.name("q"));
        // wprowadz wartosc Selenium do pola
        searchField.sendKeys("Selenium");
        // zasymuluj nacisniecie przycisku Enter
        searchField.sendKeys(Keys.ENTER);
        // znalezc rezultat
        WebElement result = driver.findElement(By.xpath("//a[contains(@href,'selenium.dev')]//h3"));

        Assert.assertTrue(result.isDisplayed());


    }
    @Test
    public void seleniumDemoPage() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://seleniumdemo.com/");
        //WebElement shopButton = driver.findElement(By.xpath("//span[text()='Shop']"));
        //shopButton.click();

        WebElement searchButton = driver.findElement(By.xpath("//span[@class='sr-only' and text()='Search']"));


        WebElement element = driver.findElement(By.xpath("//span[@class='sr-only' and text()='Search']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(element, element.getSize().width/2, element.getSize().height/2).click().perform(); //Kliknięcie na centrum elementu: Spróbuj kliknąć w centrum elementu, zamiast jego górnego lewego rogu.

        Actions actionsESC = new Actions(driver);
        actionsESC.sendKeys(Keys.ESCAPE).build().perform();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement shopButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Shop']")));

        //WebElement shopButton = driver.findElement(By.xpath("//span[text()='Shop']"));
        shopButton.click();

    }
    @Test
    public void localisationElementsTest() {

        ChromeOptions options = new ChromeOptions();
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://testeroprogramowania.github.io/selenium/basics.html");


        WebElement firstButton = driver.findElement(By.xpath("//button[@id='clickOnMe']"));
        //WebElement firstButtonClick = driver.findElement(By.id("clickOnMe")); //tak też można
        firstButton.click();
        Actions actionEnter = new Actions(driver);
        actionEnter.sendKeys(Keys.ENTER).build().perform();
        WebElement h1 = driver.findElement(By.xpath("//h1[text()='Witaj na stronie testowej']"));
        WebElement h1Contains = driver.findElement(By.xpath("//h1[contains(text(),'na stronie te')]"));
        //WebElement h1 = driver.findElement(By.tagName("h1")); // tak też można

        WebElement firstName = driver.findElement(By.xpath("//label[text()=' First name:']"));

        WebElement input2 = driver.findElement(By.xpath("//input[@id='fname']"));
        //WebElement input = driver.findElement(By.id("fname")); // tak też można
        input2.sendKeys("Test01");

        WebElement firstLink = driver.findElement(By.xpath("//a[@href='https://www.w3schools.com']"));
        WebElement secondLink = driver.findElement(By.xpath("//a[@href='https://www.google.com']"));

        WebElement table = driver.findElement(By.xpath("//table[@border='1']"));
        WebElement tableParams1 = driver.findElement(By.xpath("//table[@border='1']//th[text()='Month']"));
        WebElement tableParams2 = driver.findElement(By.xpath("//table[@border='1']//th[text()='Savings']"));
        WebElement tableParams3 = driver.findElement(By.xpath("//table[@border='1']//td[text()='January']"));

        WebElement selectVolvo = driver.findElement(By.xpath("//select//option[@value='volvo' and text()='Volvo']"));
        WebElement selectSaab = driver.findElement(By.xpath("//select//option[@value='saab' and text()='Saab']"));
        WebElement selectMercedes = driver.findElement(By.xpath("//select//option[@value='mercedes' and text()='Mercedes']"));
        WebElement selectAudi = driver.findElement(By.xpath("//select//option[@selected='selected' and @value='audi' and text()='Audi']"));

        WebElement checkbox = driver.findElement(By.xpath("//input[@type='checkbox']"));
        WebElement label1 = driver.findElement(By.xpath("//label[text()=' Potwierdzam 100% nieznajomość regulaminu. Kto ma czas na czytanie regulaminów']"));

        WebElement inputRadioMale = driver.findElement(By.xpath("//input[@type='radio' and @name='gender' and @value='male']"));
        WebElement inputRadioFemale = driver.findElement(By.xpath("//input[@type='radio' and @name='gender' and @value='female']"));
        WebElement inputRadioOther = driver.findElement(By.xpath("//input[@type='radio' and @name='gender' and @value='other']"));

        //WebElement form1 = driver.findElement(By.xpath("//form[text()=' Nazwa użytkownika']"));
        WebElement lastInputNazwaUzytkownika = driver.findElement(By.xpath("//input[@type='text' and @name='username' and @value='Mickey']"));
        //WebElement haslo = driver.findElement(By.xpath("//form[text()=' Hasło:']"));
        WebElement lastInputHaslo = driver.findElement(By.xpath("//input[@type='password' and @name='password' and @value='Mouse']"));
        WebElement submitButton = driver.findElement(By.xpath("//input[@type='submit' and @value='Submit']"));
        //submitButton.click();

        WebElement image = driver.findElement(By.xpath("//img[@id='smileImage' and @src='smile.png']"));

        WebElement ClickMe = driver.findElement(By.xpath("//button[@id='newPage' and text()='Click me']"));

        WebElement list1 = driver.findElement(By.xpath("//div[@id='container']//ul//li[text()=' List Item ']"));
        WebElement list2 = driver.findElement(By.xpath("//div[@id='container']//ul//li[text()=' Child ']"));

        List<WebElement> listOfAllInputs = driver.findElements(By.tagName("input"));
        System.out.println("Number of all inputs is: " + listOfAllInputs.size());



        List<WebElement> nazwa = driver.findElements(By.tagName("input"));
        for(int i =0;i<8;i++) {
            System.out.println("Element: " + i + " wynosi: " + nazwa.get(i));
        }

        //$$(".topSecret") //wyszukanie w konsoli
        //[p.topSecret] //odp
        WebElement findByClass = driver.findElement(By.className("topSecret"));

        //$$("input[name=gender]") //wyszukanie w konsoli
        //(3) [input, input, input]
        WebElement findByName = driver.findElement(By.name("gender"));

        //$$("#clickOnMe") //wyszukanie w konsoli
        //[button#clickOnMe] //odp
        WebElement findById = driver.findElement(By.id("clickOnMe"));

        driver.quit();

    }
    @Test //Xpath kontynuacja
    public void xpathKontynuacjaTest() {
        ChromeOptions options = new ChromeOptions();
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://testeroprogramowania.github.io/selenium/basics.html?username=Mickey&password=Mouse");

        //$x("//*") //przeszukanie wszystkich selectorów w konsolu w DevTools
        List<WebElement> allElements = driver.findElements(By.xpath("//*"));
        System.out.println("Liczba elementów: " + allElements.size());

        //$x("//input") //wylistowanie wszystkich inputów
        //(8) [input#fname, input, input, input, input, input, input, input]
        WebElement elementInput = driver.findElement(By.xpath("//input"));

        //$x("//input[2]") //wybór inputu o nr 2 z powyżej wybranych
        //(2) [input, input]
        WebElement elementInput_2 = driver.findElement(By.xpath("(//input)[2]"));

        //$x("(//input)[last()]") //wybór ostatniego
        //[input]
        WebElement lastElementInput = driver.findElement(By.xpath("(//input)[last()]"));

        //$x("//*[@name]") //wybór wszystkich elementów które mają atrybut name
        //(6) [input#fname, input, input, input, input, input]
        List<WebElement> allNameElements = driver.findElements(By.xpath("(//*)[@name]"));

        //$x("//*[@name='fname']") //wybór wszystkich elementów które mają atrybut name=fname
        //[input#fname]
        List<WebElement> allNameFnameElements = driver.findElements(By.xpath("(//*)[@name='fname']")); // metoda findElements nie wyrzuca wyjątku jesli nic nie znajdzie!! będzie po prostu pusta lista

        //$x("//button[@id]") //wybór konkretnego selectora button, który ma w sobie id=clickOnMe
        //(2) [button#clickOnMe, button#newPage]
        WebElement buttonElement = driver.findElement(By.xpath("//button[@id='clickOnMe']"));

        //$x("//*[starts-with(@name, 'user')]") //wyszukiwanie elementów, których atrybut name zaczyna się od user
        //[input]
        WebElement elementStartWith = driver.findElement(By.xpath("//*[starts-with(@name, 'user')]")); //ends-with jest w xpath 2.0







        driver.quit();
    }

    @Test //selektory CSS
    public void localisationElementsTest2() {
        ChromeOptions options = new ChromeOptions();
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://testeroprogramowania.github.io/selenium/basics.html");

        WebElement element1 = driver.findElement(By.cssSelector(".topSecret"));


        By all = By.cssSelector("*");
        driver.findElement(all);

        By ulInsideDiv = By.cssSelector("div ul");
        By trInTable = By.cssSelector("table tr");
        By trInBody = By.cssSelector("tbody tr");
        driver.findElement(ulInsideDiv);
        driver.findElement(trInTable);
        driver.findElement(trInBody);

        By allLi = By.cssSelector("div li");
        driver.findElement(allLi);

        List<WebElement> wszstkieLi = driver.findElements(By.cssSelector("div li")); //jak sprawdzić ile jest wszystkich cssSelectorów 'Li' ??
        System.out.println("Liczba li: " + wszstkieLi.size());
        //$$("div li") //wyszukanie w konsoli DevTools
        //(5) [li, li, li, li, li] //odp 5

        List<WebElement> wszstkieUl = driver.findElements(By.cssSelector("div ul")); //jak sprawdzić ile jest wszystkich cssSelectorów 'Ul' ??
        System.out.println("Liczba Ul: " + wszstkieUl.size());
        //$$("div ul") //wyszukanie w konsoli DevTools
        //(2) [ul, ul] //odp 2

        //ile jest elementów 'ul' bezpośrednio po div ?
        By zagniezdzoneUl = By.cssSelector("div>ul");
        driver.findElement(zagniezdzoneUl);
        List<WebElement> zagnieLUl = driver.findElements(zagniezdzoneUl);
        System.out.println("Liczba ul to: " + zagnieLUl.size());
        //$$("div>ul") //wyszukanie w konsoli DevTools
        //[ul] //odp 1

        //pierwszy formularz po tagu label
        By firstForm = By.cssSelector("label + form");
        driver.findElement(firstForm);
        //$$("label + form") //wyszukiwanie w konsoli DevTools
        //[form] //odp

        //Ogólnie sa dwa tagi
        //$$("label ~ form")
        //(2) [form, form] //to jest odp czyli 2
        By firstFormAll = By.cssSelector("label ~ form");
        List<WebElement> listFirstFormAll = driver.findElements(firstFormAll);
        System.out.println("Liczba wszystkich \'form\' po tagu label to: " + listFirstFormAll.size());
        Assert.assertEquals(listFirstFormAll.size(), 2);




        WebElement attrTag = driver.findElement(By.cssSelector("input[name='fname']"));
        //$$("input[name='fname']") //zapytanie w konsoli webTools
        //[input#fname] //odp

        WebElement attrContains = driver.findElement(By.cssSelector("input[name*='nam']"));
        //$$("input[name*='nam']") //zapytanie w konsoli webTools
        //(2) [input#fname, input] //odp

        WebElement attrBeginWith = driver.findElement(By.cssSelector("input[name^='fna']"));
        //$$("input[name^='fna']") //zapytanie w konsoli webTools
        //[input#fname] //odp

        WebElement attrEndWith = driver.findElement(By.cssSelector("input[name$='ame']"));
        //$$("input[name$='ame']") //zapytanie w konsoli webTools
        //(2) [input#fname, input] //odp

        WebElement ahrefLocalisation = driver.findElement(By.cssSelector("a[href*='w3sch']"));

        driver.quit();

    }

    @Test //selectory CSS childs
    public void childCssSelectorTest() {
        ChromeOptions options = new ChromeOptions();
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://testeroprogramowania.github.io/selenium/basics.html?username=Mickey&password=Mouse");

        //lokalizacja tagu child, który ejst  pierwszym elementem tagu nadrzednego:
        //$$("li:first-child") //wysuzkanie w koncoli w devTools
        //(2) [li, li] //odp
        WebElement firstChild = driver.findElement(By.cssSelector("li:first-child"));

        //$$("li:last-child") //tak samo tylko ostatni child
        //(2) [li, li]
        WebElement lastChild = driver.findElement(By.cssSelector("li:last-child"));

        //$$("li:nth-child(2)") //wyszukanie dziecka po konkretnym numerze id (np dziecko nr 2)
        //[li] //odp
        WebElement selectedChild = driver.findElement(By.cssSelector("li:nth-child(2)"));

        driver.quit();
    }
    @Test
    public void googleTest() {

        ChromeOptions options = new ChromeOptions();
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://google.com");

        WebElement firstButtonAccept = driver.findElement(By.id("L2AGLb"));
        firstButtonAccept.click();

        WebElement searchBarClassName = driver.findElement(By.className("gLFyf"));
        WebElement seachBarXpath = driver.findElement(By.xpath("//div//input[@class='gLFyf']"));

        searchBarClassName.sendKeys("Kasztan");
        Actions actionEnter = new Actions(driver);
        actionEnter.sendKeys(Keys.ENTER).build().perform();

        driver.navigate().back();
        WebElement buttonSzukajWGoogle = driver.findElement(By.xpath("//div/center//input[@class='gNO89b' and @value='Szukaj w Google']"));

        WebElement logoGoogle = driver.findElement(By.xpath("//img[@class='lnXdpd' and @alt='Google' and @height='92']"));

        //$$("img[class*='nX']")
        //[img.lnXdpd]
        WebElement logoGoogleCSS = driver.findElement(By.cssSelector("img[class*='nX']"));
        //$$("img[class^=lnX]")
        //[img.lnXdpd]
        WebElement logoGoogleCSS2 = driver.findElement(By.cssSelector("img[class^='lnX']"));
        //$$("img[class$='pd']")
        //[img.lnXdpd]
        WebElement logoGoogleCSS3 = driver.findElement(By.cssSelector("img[class$='pd']"));





        driver.quit();

    }

    //Sum up:
    //przeszukiwanie po id jest najszybsze i id powinno zawsze być unikalne! (spr czy nie jest auto generowane) By.id
    //przeszukiwanie po name (name niekoniecznie jest unikalne) By.name
    //wyszukiwanie linków By.linkText i By.parrtialLinkText
    //wyszukiwanie po selektorze css
    //wyszuiwanie po Xpath jest najwolniejsze ale pozwala na najbardziej rozbudowane wyszukiwanie



}
