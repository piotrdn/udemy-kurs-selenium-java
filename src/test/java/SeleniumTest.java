import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.testng.annotations.Test;

import java.sql.Driver;

public class SeleniumTest {

    @Test
    public void openGooglePage() {

        WebDriver driver = getDriver("chrome");
        driver.get("https://google.com");

    }



    public WebDriver getDriver(String browser) {
        if(browser == "chrome"){
            String path = "src/main/resources/chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", path);
            return new ChromeDriver();
        } else if (browser == "firefox") {
            String pathFirefox = "src/main/resources/geckodriver.exe";
            System.setProperty("webdriver.gecko.driver", pathFirefox);
            return new FirefoxDriver();
        } else if (browser == "IE") {
            String pathIE = "src/main/resources/IEDriverServer.exe";
            System.setProperty("webdriver.ie.driver", pathIE);
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.withInitialBrowserUrl("http://google.com");
            return new InternetExplorerDriver(options);
        }
        else{
            throw new InvalidArgumentException("Invalid value!!!");
        }
        //return null;

    }



}
