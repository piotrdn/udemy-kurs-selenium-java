import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.List;

public class SelectCheckHomeWork {

    @Test
    public void testMethod() {
        Boolean result = selectCheck("Mercedes");
        System.out.println();
        System.out.println("The main result: " + result);
    }


    public Boolean selectCheck(String inputValue) {
        Boolean state = true;
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://testeroprogramowania.github.io/selenium/basics.html");

        WebElement mySelect = driver.findElement(By.cssSelector("select"));
        Select allOptions = new Select(mySelect);
        List<WebElement> listOfOptions = allOptions.getOptions();
        System.out.println("Number of all options: " + listOfOptions.size());

        int i = -1;
        for(WebElement options: listOfOptions) {
            System.out.println(options.getText());

            i++;
            System.out.println("listOfOptions: " + listOfOptions.get(i).getText() + " vs inputValue: " + inputValue);
            if(listOfOptions.get(i).getText().contains(inputValue)){
                state = true;
                break;
            }
            else state = false;

        }

        driver.quit();

        return state;
    }
}
